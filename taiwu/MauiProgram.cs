﻿using taiwu.ViewModels;
using taiwu.Views;

namespace taiwu;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			});
		builder.Services.AddSingleton(Plugin.Maui.Audio.AudioManager.Current);
        builder.Services.AddSingleton<MineViewModel>();
        builder.Services.AddSingleton<MinePage>();
        return builder.Build();
	}
}
