﻿namespace taiwu.Models
{
    public class Song
    {
        public Song()
        {
            Title = $"{Path.GetRandomFileName()}.notes.txt";
            Artist = "";
        }
        public string Title { get; set; }
        public string Artist { get; set; }
        public void Save() =>
            File.WriteAllText(Path.Combine(FileSystem.AppDataDirectory, Title), Artist);

        public void Delete() =>
            File.Delete(Path.Combine(FileSystem.AppDataDirectory, Title));
        public static Song Load(string title)
        {
            title = Path.Combine(FileSystem.AppDataDirectory, title);

            if (!File.Exists(title))
                throw new FileNotFoundException("Unable to find file on local storage.", title);

            return
                new()
                {
                    Title = Path.GetFileName(title),
                    Artist = File.ReadAllText(title),
                };
        }
        public static IEnumerable<Song> LoadAll()
        {
            // Get the folder where the notes are stored.
            string appDataPath = FileSystem.AppDataDirectory;

            // Use Linq extensions to load the *.notes.txt files.
            return Directory

                    // Select the file names from the directory
                    .EnumerateFiles(appDataPath, "*.notes.txt")

                    // Each file name is used to load a note
                    .Select(filename => Song.Load(Path.GetFileName(filename)))

                    // With the final collection of notes, order them by date
                    .OrderByDescending(note => note.Title);
        }
    }
}
