﻿using CommunityToolkit.Mvvm.Input;
using Plugin.Maui.Audio;
using System.Collections.ObjectModel;
using System.Windows.Input;
using taiwu.Models;

namespace taiwu.ViewModels
{
    public class MineViewModel : IQueryAttributable
    {
        public ObservableCollection<DetailViewModel> AllSongs { get; }
        public ICommand NewCommand { get; }
        public ICommand SelectSongCommand { get; }
        public ICommand PlayCommand { get; private set; }
        public IAudioPlayer player;
        public string Title { get; private set; }
        public string Artist { get; private set; }
        public string ImageSource { get; private set; }
        private Plugin.Maui.Audio.IAudioManager audioManager;

        public MineViewModel(IAudioManager audioManager)
        {
            ImageSource = "icon_music.png";
            Title = "遇见";
            Artist = "孙燕姿";
            this.audioManager = audioManager;
            AllSongs = new ObservableCollection<DetailViewModel>(Song.LoadAll().Select(n => new DetailViewModel(n)));
            NewCommand = new AsyncRelayCommand(NewSongAsync);
            SelectSongCommand = new AsyncRelayCommand<DetailViewModel>(SelectSongAsync);
            PlayCommand = new AsyncRelayCommand(PlayAsync);
        }
        private async Task PlayAsync()
        {
            player = audioManager.CreatePlayer(await FileSystem.OpenAppPackageFileAsync("Runaway.mp3"));
            player.Play();
        }
        private async Task NewSongAsync()
        {
            await Shell.Current.GoToAsync(nameof(Views.DetailPage));
        }

        private async Task SelectSongAsync(DetailViewModel song)
        {
            if (song != null)
                await Shell.Current.GoToAsync($"{nameof(Views.DetailPage)}?load={song.Identifier}");
        }

        void IQueryAttributable.ApplyQueryAttributes(IDictionary<string, object> query)
        {
            if (query.ContainsKey("deleted"))
            {
                string songId = query["deleted"].ToString();
                DetailViewModel matchedSong = AllSongs.Where((n) => n.Identifier == songId).FirstOrDefault();

                // If song exists, delete it
                if (matchedSong != null)
                    AllSongs.Remove(matchedSong);
            }
            else if (query.ContainsKey("saved"))
            {
                string songId = query["saved"].ToString();
                DetailViewModel matchedSong = AllSongs.Where((n) => n.Identifier == songId).FirstOrDefault();

                // If song is found, update it
                if (matchedSong != null)
                {
                    matchedSong.Reload();
                    AllSongs.Move(AllSongs.IndexOf(matchedSong), 0);
                }

                // If song isn't found, it's new; add it.
                else
                    AllSongs.Insert(0, new DetailViewModel(Models.Song.Load(songId)));
            }
        }
    }
}


