﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System.Windows.Input;
using static System.Net.Mime.MediaTypeNames;

namespace taiwu.ViewModels
{
 public   class DetailViewModel : ObservableObject, IQueryAttributable
    {
        private Models.Song song;
        public string Artist
        {
            get => song.Artist;
            set
            {
                if (song.Artist!= value)
                {
                    song.Artist = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Identifier => song.Title;

        public ICommand SaveCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public DetailViewModel()
        {
            this.song = new Models.Song();
            SaveCommand = new AsyncRelayCommand(Save);
            DeleteCommand = new AsyncRelayCommand(Delete);
        }

        public DetailViewModel(Models.Song song)
        {
            this.song = song;
            SaveCommand = new AsyncRelayCommand(Save);
            DeleteCommand = new AsyncRelayCommand(Delete);
        }
        private async Task Save()
        {
            song.Save();
            await Shell.Current.GoToAsync($"..?saved={song.Title}");
        }

        private async Task Delete()
        {
            song.Delete();
            await Shell.Current.GoToAsync($"..?deleted={song.Title}");
        }

        public void ApplyQueryAttributes(IDictionary<string, object> query)
        {
            if (query.ContainsKey("load"))
            {
                song = Models.Song.Load(query["load"].ToString());
                RefreshProperties();
            }
        }
        public void Reload()
        {
            song = Models.Song.Load(song.Title);
            RefreshProperties();
        }

        private void RefreshProperties()
        {
            OnPropertyChanged(nameof(Text));
        }
    }
}
