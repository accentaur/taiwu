using taiwu.ViewModels;

namespace taiwu.Views;

public partial class MinePage : ContentPage
{

    public MinePage(MineViewModel mineViewModel)
    {
        InitializeComponent();
        BindingContext = mineViewModel;
    }
    private void ContentPage_NavigatedTo(object sender, NavigatedToEventArgs e)
    {
        LoveCollection.SelectedItem = null;
    }
}